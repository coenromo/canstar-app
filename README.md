# CanstarApp

1. Use mat-table to bind property elements directly to columns. Better than multiple ngFor loops and arrays 
2. Use dogService to mock data
3. Create list and detail component and add new dogs back to detail component 
4. Use feature module in footer of web app
5. Use CSS animations to transform footer element 

## Developer Technical Questions

###	Explain how routing works in Angular. 
The angular router defines which views are shown to the user based on the URL. Angular uses the base href to build navigation URLs based on the paths defined in the routing module. Routing can be navigated to from direct URL input, page links, or by using the browser back and forward buttons. 

###	An application requires access to database credentials at run-time, how do you manage these credentials securely (hint: not freely available, stored in source control)?
This solution relies on a number of best practices that include: 
1. Don't store credentials in plain text
2. Encrypt credentials where possible
3. Use a hard key (USB etc) for servers/hardware or a soft key for online apps
4. Require the application to hash the credentials so they are more secure

The ideal solution would be to break down your systems architecture into logical components so that there are multiple layers of encryption and hashing between communications. This would ensure that each system had their own layer of protection on top of the credentials required from the database. You could deny direct access to the database without first requiring a key from the middleware application, which would subsequently require a key from the UI in order to access the database.

###	Describe stateless web applications. What are the benefits?
Stateless web applications do not store any user data in subsequent sessions with the server. They utilise REST API design, and reduce memory usage and storage by requiring data from the server each time a new session is created. This means it is easier to scale these applications as the server doesn't need to manage user sessions, and the application is usually much less complex as the web application doesn't need to control the state.  

### Describe microservices and its benefits?
Microservices are a style of building applications as a set of small services that together create the entire application. These services are usually modular applications that can be run independently or as part of the stack. This means companies can scale apps individually without having reliance on the overarching infrastructure, while also keeping these smaller services lean and dynamic. Microservices are decentralized, and can be written in different coding languages, allowing greater flexibility in application design. 

###	Describe separation of concerns?
The seperation of concerns principle tells us to seperate application logic into distinct sections so they can each address a different concern. This allows each component to focus on a single purpose, and allows components to be reused across the rest of the application. This can also be seen in the lowest form of software development, where seperation of concerns would tell us that long and complex functions should be broken down and refactored to increase code clarity. Seperation of concerns allows a higher level of control over each seperation while also ensuring the application is easier to maintain by reducing the amount of code that has to be modified.

###	What flavours of Linux have you used?
1. Ubuntu 
2. Red Hat Enterprise Linux

###	On a scale of 1-10, please rate your experience with: 
1. Docker, Cloud (AWS)
    * Docker - 4
    * AWS - 2
2. Version Control - 7
3. Continuous Integration, Automated testing, DevOps - 6
4. Documentation, commenting code, formal documentation - 7

