import { Component, OnInit, Input } from "@angular/core";
import { Product } from "../product";
import { ActivatedRoute } from "@angular/router";
import { Dogs } from "../dogs";
@Component({
  selector: "product-detail",
  templateUrl: "./product-detail.component.html",
  styleUrls: []
})
export class ProductDetailComponent implements OnInit {
  
  //get data from parent using Input
  @Input() dogs: Dogs;

  constructor() {}
  ngOnInit() {
    
  }
}
