/**
 * dogs service 
 */
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
/**
 * import data
 */
import { Dogs } from './dogs';
import { DOGGOS } from './mock-dogs';

@Injectable({
  providedIn: 'root'
})
export class DogsService {
//return dogs from mock dogs 
  getDogs(): Observable<Dogs[]> {
    const doggos = of(DOGGOS);
    return of(DOGGOS);
  }
//add dog to service 
  addDogs(id, name) {
    this.getDogs();
    DOGGOS.push(id, name)
  }
}