import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-table-dialog',
  templateUrl: './table-dialog.component.html',
  styleUrls: ['./table-dialog.component.css']
})
export class TableDialogComponent implements OnInit {

  name: string;
  animal: string;
  idBool: boolean; //to remove ID but keep it hidden in form to pass data back to
  numberRegEx = /^[0-9]*$/; //regex to catch number ***also matches =, + etc after number

  //form control creation 
  nameForm = new FormControl('', [Validators.required]);
  weightForm = new FormControl('', [Validators.required, Validators.pattern(this.numberRegEx)]);
  
  constructor(public dialogRef: MatDialogRef<TableDialogComponent>,
            @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    
  }

  //error message for nameForm
  getErrorMessage() {
    if (this.nameForm.hasError('required')) {
      return 'You must enter a value';
    }
    return 'String can be any, will never get hit';
  }

  //error message for number input 
  getNumberMessage() {
    if (this.weightForm.hasError('required')) {
      return 'Weight is required';
    }
    return 'Weight must be a number';
  }

}
