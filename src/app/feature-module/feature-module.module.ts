import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureModuleComponent } from './feature-module/feature-module.component';

/**
 * Feature module to use on footer of site 
 */

@NgModule({
  declarations: [FeatureModuleComponent],
  imports: [
    CommonModule
  ],
  exports: [
    FeatureModuleComponent
  ]
})
// just export empty, html will render from feature-module
export class FeatureModuleModule { }
