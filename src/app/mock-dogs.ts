import { Dogs } from './dogs';

export const DOGGOS: Dogs[] = [
  { id: 1, name: 'Ivy' },
  { id: 2, name: 'Vin' },
  { id: 3, name: 'Gus' },
  { id: 4, name: 'Loui' },
  { id: 5, name: 'Archie' },
  { id: 6, name: 'Coop' }
];