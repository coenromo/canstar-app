import { Component, Inject, ViewChild } from '@angular/core'; 
import { Product } from "./product";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TableDialogComponent } from './table-dialog/table-dialog.component';
import { MatTable } from '@angular/material/table';

//dialog definitions
export interface DialogData {
  animal: string;
  name: string;
}

//define table columns 
export interface PeriodicElement {
  ID: number;
  Name: string;
  Breed: string;
  Weight: number;
  Pup: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  /**
   * table data and declerations
   */
  displayColumns = ['ID', 'Name', 'Breed', 'Weight', 'Pup'];
  array = TABLE_DATA;

  /**
 * get ref to table 
 */
  @ViewChild(MatTable) table: MatTable<any>;

  //dialog
  id: number;
  name: string;
  breed: string;
  weight: string;
  pup: string;

  //dialog constructor
  constructor(public dialog: MatDialog) {}

  // dummy event handler
  openDialog(): void {
    console.log("dialogPress");
    const dialogRef = this.dialog.open(TableDialogComponent, {
      width: '250px',
      data: { 
        ID: this.id,
        Name: this.name,
        Breed: this.breed,
        Weight: this.weight,
        Pup: this.pup 
              }
    });

    dialogRef.afterClosed().subscribe(result => {
      //get current ID number
      let idLength = this.array.length;
      idLength++; //incriment 
      //can check for anything now that UI validation is handled
      if (!result.Breed) {
        //do nothing
      } else {
        result.ID = idLength;
        this.array.push(result);
        //won't have empty data now that UI validation is implemented 
        this.refreshTable();
      }
      console.log('The dialog was closed', result);
    });
  }

  refreshTable() {
    //refresh table data
    console.log("table refresh");
    this.table.renderRows();
  }
}

// @Component({
//   selector: 'table-dialog.component',
//   templateUrl: 'table-dialog.component.html',
// })
// export class TableDialogComponent {

//   constructor(
//     public dialogRef: MatDialogRef<TableDialogComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

//   onNoClick(): void {
//     this.dialogRef.close();
//   }

// }

//define table elements 
let TABLE_DATA: PeriodicElement[] = [
  {ID: 1, Name: 'Ivy', Breed: 'Great Dane/Lab', Weight: 40, Pup: 'Beaun'},
  {ID: 2, Name: 'Scout', Breed: 'Mini Foxie', Weight: 4, Pup: 'Grumpy boi'},
  {ID: 3, Name: 'CoopDawg', Breed: 'Jack Russell', Weight: 8, Pup: 'Fatso'},
  {ID: 4, Name: 'Gus', Breed: 'German Shephard', Weight: 50, Pup: 'Puppo'},
  {ID: 5, Name: 'Loui', Breed: 'Dingo', Weight: 25, Pup: 'Doggo'},
];


