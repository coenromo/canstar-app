import { Component, OnInit } from "@angular/core";
import { Product } from "../product";
import { DogsService } from "../dogs.service";
import { Dogs } from "../dogs";

@Component({
  selector: "product-list",
  templateUrl: "product-list.component.html",
  styleUrls: ["product-list.component.css"]
})

export class ProductListComponent implements OnInit {
  doggos: Dogs[] = [];
  selectedDogs?: Dogs;

  //get dogs from DogsService
  constructor(private dogService: DogsService) { }

  ngOnInit() {
    this.getDogs();
  }

  //get mock dog data
  getDogs(): void {
    this.dogService.getDogs()
    .subscribe(doggos => this.doggos = doggos);
  }

  //onselect list item
  onSelect(dogs: Dogs): void {
    this.selectedDogs = dogs;
  }

  //add name from input field
  add(name: string): void {
    name = name.trim();
    let updateId = this.doggos.length; //get ID
    updateId++; //increment 
    if (!name) { return; }
    this.doggos.push({id: updateId, name: name}); //push 
    //try add to service but run out of time
    //this.dogService.addDogs(updateId, name);
  }
}